package net.sf.vgrs.moneyTransfer.service;

import net.sf.vgrs.moneyTransfer.dao.AccountDao;
import net.sf.vgrs.moneyTransfer.dao.ConnectionManager;
import net.sf.vgrs.moneyTransfer.dao.jdbc.AccountDaoJdbcImpl;
import net.sf.vgrs.moneyTransfer.dao.jdbc.DataSourceManager;
import net.sf.vgrs.moneyTransfer.dao.jdbc.H2ContextDataSourceManager;
import net.sf.vgrs.moneyTransfer.dao.jdbc.JDBCConnectionManager;
import net.sf.vgrs.moneyTransfer.domain.Account;
import net.sf.vgrs.moneyTransfer.domain.MoneyTransactionInfo;
import net.sf.vgrs.moneyTransfer.utlis.Constants;
import net.sf.vgrs.moneyTransfer.utlis.UserDefinedException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MoneyTransactionServiceTest {

    private static final Long existingAccountNum = 1000L;
    private static final BigDecimal currentBalance = BigDecimal.valueOf(100);

    private static ArgumentMatcher<BigDecimal> lessOrEqualToCurrentBalance = t -> (t.compareTo(currentBalance) == -1 || t.compareTo(currentBalance) == 0);
    private static ArgumentMatcher<BigDecimal> greatherThanCurrentBalance = t -> t.compareTo(currentBalance) == 1;
    private static final Long nonExistingAccountNum = 2000L;


    private ConnectionManager connectionManager;
    private AccountDao accountDao;


    private MoneyTransactionService service;

    @BeforeEach
    void setUp() throws UserDefinedException {
        //DataSourceManager dataSourceManager = new H2ContextDataSourceManager("", "", "");
        connectionManager = Mockito.mock(JDBCConnectionManager.class);
        accountDao = Mockito.mock(AccountDaoJdbcImpl.class);
        service = new MoneyTransactionService(connectionManager, accountDao);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    @DisplayName("Test block method for no existing account")
    void blockShouldFailWhenAccountNotExists() throws UserDefinedException {
        when(accountDao.getAccountInfo(nonExistingAccountNum)).thenThrow(
                new UserDefinedException(UserDefinedException.Errors.ERROR_BUSINESS_LEVEL));
        Assertions.assertThrows(UserDefinedException.class, () -> {
            service.block(nonExistingAccountNum, 200L, BigDecimal.valueOf(300));
        });
    }

    @Test
    @DisplayName("Test block method for no existing account and not enough money")
    void blockShouldFailWhenAccNotExistsAndAmountIsNotEnough() throws UserDefinedException {
        when(accountDao.getAccountInfo(nonExistingAccountNum)).thenThrow(
                new UserDefinedException(UserDefinedException.Errors.ERROR_BUSINESS_LEVEL));
        UserDefinedException userDefinedException = Assertions.assertThrows(UserDefinedException.class, () -> {
            service.block(nonExistingAccountNum, 200L, currentBalance.add(BigDecimal.valueOf(1)));
        });

        Assertions.assertEquals(UserDefinedException.Errors.ERROR_BUSINESS_LEVEL, userDefinedException.getError());
    }

    @Test
    @DisplayName("Test block method for existing account and not enough money")
    void blockShouldFailWhenAccExistsAndAmountIsNotEnough() throws UserDefinedException {
        when(accountDao.getAccountInfo(existingAccountNum)).thenReturn(new Account(
                existingAccountNum, Constants.Currencies.USD, 1, currentBalance));
        UserDefinedException userDefinedException = Assertions.assertThrows(UserDefinedException.class, () -> {
            service.block(existingAccountNum, 200L, currentBalance.add(BigDecimal.valueOf(1)));
        });

        Assertions.assertEquals(UserDefinedException.Errors.ERROR_BUSINESS_LEVEL, userDefinedException.getError());
    }

    @Test
    @DisplayName("Test block method for existing account and enough money")
    void blockShouldFailWhenAccExistsAndAmountIsEnough() throws UserDefinedException {
        BigDecimal amount = currentBalance.subtract(BigDecimal.valueOf(1));

        when(accountDao.getAccountInfo(existingAccountNum)).thenReturn(new Account(
                existingAccountNum, Constants.Currencies.USD, 1, currentBalance));

        when(accountDao.block(eq(existingAccountNum), anyLong(), argThat(lessOrEqualToCurrentBalance)))
                .thenAnswer(t -> new MoneyTransactionInfo(
                        9000L,
                        existingAccountNum,
                        ((Long) t.getArguments()[1]),
                        ((BigDecimal) t.getArguments()[2])));

        when(accountDao.updateAccountBalance(existingAccountNum, amount.multiply(BigDecimal.valueOf(-1)))).thenReturn(true);

        MoneyTransactionInfo blockInfo = service.block(existingAccountNum,200L, amount);

        Assertions.assertEquals(existingAccountNum, blockInfo.getAccountNumber(), "Account number should be same as we sent");
        Assertions.assertEquals(amount, blockInfo.getAmount(), "Operation amount should be same as we sent");
    }


}