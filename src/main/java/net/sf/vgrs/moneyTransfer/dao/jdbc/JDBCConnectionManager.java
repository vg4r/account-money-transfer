package net.sf.vgrs.moneyTransfer.dao.jdbc;

import net.sf.vgrs.moneyTransfer.dao.ConnectionManager;
import net.sf.vgrs.moneyTransfer.utlis.UserDefinedException;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

import static net.sf.vgrs.moneyTransfer.utlis.Constants.ANNOTATION_JDBC_DATASOURCE_MAN;

@Singleton
public class JDBCConnectionManager extends ConnectionManager {

    private Connection connection;
    private DataSource dataSource;


    @Inject
    public JDBCConnectionManager(@Named(ANNOTATION_JDBC_DATASOURCE_MAN)
                                             DataSourceManager dataSourceManager ) {
        try {
            this.dataSource = dataSourceManager.get();
        } catch (UserDefinedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void openConnection() throws UserDefinedException {
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new UserDefinedException(UserDefinedException.Errors.ERROR_DB_CONNECTION, e);
        }
    }

    @Override
    public void closeConnection() throws UserDefinedException {
        try {
            connection.close();
        } catch (SQLException e) {
            throw new UserDefinedException(UserDefinedException.Errors.ERROR_DB_CONNECTION, e);
        }
    }

    @Override
    public void commit() throws UserDefinedException {
        try {
            connection.commit();
        } catch (SQLException e) {
            throw new UserDefinedException(UserDefinedException.Errors.ERROR_DB_CONNECTION, e);
        }
    }

    @Override
    public void rollback() throws UserDefinedException {
        try {
            connection.rollback();
        } catch (SQLException e) {
            throw new UserDefinedException(UserDefinedException.Errors.ERROR_DB_CONNECTION, e);
        }
    }

    public Connection getConnection() {
        return connection;
    }
}
