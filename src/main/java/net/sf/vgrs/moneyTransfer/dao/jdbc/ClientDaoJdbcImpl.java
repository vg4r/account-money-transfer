package net.sf.vgrs.moneyTransfer.dao.jdbc;

import net.sf.vgrs.moneyTransfer.dao.AbstractDao;
import net.sf.vgrs.moneyTransfer.dao.ClientDao;
import net.sf.vgrs.moneyTransfer.dao.ConnectionManager;
import net.sf.vgrs.moneyTransfer.domain.Client;
import net.sf.vgrs.moneyTransfer.utlis.UserDefinedException;

import javax.inject.Inject;
import javax.inject.Named;
import java.sql.*;
import java.util.List;

import static net.sf.vgrs.moneyTransfer.utlis.Constants.ANNOTATION_JDBC_CONNECTION_MAN;

public class ClientDaoJdbcImpl extends AbstractDao implements ClientDao {

    private JDBCConnectionManager cm;

    @Inject
    public ClientDaoJdbcImpl(@Named(ANNOTATION_JDBC_CONNECTION_MAN)
                                     ConnectionManager connectionManager) {
        super(connectionManager);
        this.cm = (JDBCConnectionManager) connectionManager;
    }


    @Override
    public Client addClient(Client client) throws UserDefinedException {
        try {
            PreparedStatement ps = cm.getConnection()
                    .prepareStatement(
                            "insert into acct.clients(full_name) values (?)",
                            Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, client.getFullName());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            while ( rs.next() ) {
                client.setId(rs.getLong( 1 ));
            }
            return client;
        } catch (SQLException ex) {
            throw new UserDefinedException(
                    UserDefinedException.Errors.ERROR_DB_CALL, ex);
        }
    }

    @Override
    public List<Client> getClients() throws UserDefinedException {
        throw new UserDefinedException(UserDefinedException.Errors.ERROR_NOT_IMPLEMENTED);
    }

    @Override
    public Client getClient() throws UserDefinedException {
        throw new UserDefinedException(UserDefinedException.Errors.ERROR_NOT_IMPLEMENTED);
    }

    @Override
    public boolean deleteClient(long clientId)throws UserDefinedException{
        throw new UserDefinedException(UserDefinedException.Errors.ERROR_NOT_IMPLEMENTED);
    }
}
