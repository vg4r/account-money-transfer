package net.sf.vgrs.moneyTransfer.utlis;

import java.io.IOException;
import java.util.Properties;

public class Utility {

    public static Properties loadPropertiesFromResource() throws IOException {
        Properties properties = new Properties();
        properties.load(Utility.class.getResourceAsStream("/config/application.properties"));
        return properties;
    }

}
