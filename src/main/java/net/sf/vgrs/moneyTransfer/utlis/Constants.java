package net.sf.vgrs.moneyTransfer.utlis;

public class Constants {

    public static final String ANNOTATION_ACCOUNT_DAO_JDBC = "account-dao-jdbc";
    public static final String ANNOTATION_CLIENT_DAO_JDBC = "client-dao-jdbc";
    public static final String ANNOTATION_JDBC_CONNECTION_MAN = "jdbc-connection-manager";
    public static final String ANNOTATION_JDBC_DATASOURCE_MAN = "jdbc-datasource-manager";


    public static final String TAG_STATUS = "#status";
    public static final String TAG_REQ_CODE  = "#request-code";

    public enum Currencies {
        USD,
        PLN
    }

    public static class AppConfigKeys{
        public static final String APP_DB_JDBC_H2_URL = "app.db.jdbc.h2.url";
        public static final String APP_DB_JDBC_H2_USERNAME = "app.db.jdbc.h2.username";
        public static final String APP_DB_JDBC_H2_PASSWORD = "app.db.jdbc.h2.password";
    }
}
