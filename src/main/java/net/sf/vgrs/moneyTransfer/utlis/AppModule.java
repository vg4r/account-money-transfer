package net.sf.vgrs.moneyTransfer.utlis;

import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.name.Names;
import net.sf.vgrs.moneyTransfer.dao.AccountDao;
import net.sf.vgrs.moneyTransfer.dao.ClientDao;
import net.sf.vgrs.moneyTransfer.dao.ConnectionManager;
import net.sf.vgrs.moneyTransfer.dao.jdbc.*;
import net.sf.vgrs.moneyTransfer.service.MoneyTransactionService;
import net.sf.vgrs.moneyTransfer.web.controller.MoneyTransactionController;
import net.sf.vgrs.moneyTransfer.web.controller.ClientController;
import java.io.IOException;

import static net.sf.vgrs.moneyTransfer.utlis.Constants.*;

public class AppModule implements Module
{
    public void configure(final Binder binder) {
        binder.bind(DataSourceManager.class)
                .annotatedWith(Names.named(ANNOTATION_JDBC_DATASOURCE_MAN))
                .to(H2ContextDataSourceManager.class);

        binder.bind(ConnectionManager.class)
                .annotatedWith(Names.named(ANNOTATION_JDBC_CONNECTION_MAN))
                .to(JDBCConnectionManager.class);

        binder.bind(MoneyTransactionController.class);
        binder.bind(ClientController.class);
        binder.bind(MoneyTransactionService.class);

        binder.bind(AccountDao.class)
                .annotatedWith(Names.named(ANNOTATION_ACCOUNT_DAO_JDBC))
                .to(AccountDaoJdbcImpl.class);

        binder.bind(ClientDao.class)
                .annotatedWith(Names.named(ANNOTATION_CLIENT_DAO_JDBC))
                .to(ClientDaoJdbcImpl.class);

        try {
            Names.bindProperties(binder, Utility.loadPropertiesFromResource());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
