package net.sf.vgrs.moneyTransfer.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

public class UserDefinedResponse<T> {
    private final Header header;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T body;

    private UserDefinedResponse(Header header) {
        this.header = header;
    }

    private UserDefinedResponse(Header header, T body) {
        this.header = header;
        this.body = body;
    }

    public static <T> UserDefinedResponse<T> create(Header header){
        return new UserDefinedResponse<T>(header);
    }

    public Header getHeader() {
        return header;
    }

    public UserDefinedResponse<T> body(T body) {
        this.body = body;
        return this;
    }

    public T getBody() {
        return body;
    }

    public static class Header {

        private final String status;
        private final String text;

        public Header(Status status, String text) {
            this.status = status.name();
            this.text = text;
        }


        public static Header success(){
            return new Header(Status.SUCCESS, "");
        }

        public static Header warning(){
            return new Header(Status.WARNING, "");
        }

        public static Header error(){
            return error("");
        }

        public static Header error(Exception e){
            return error(e.getMessage());
        }

        public static Header error(String error){
            return new Header(Status.ERROR, error);
        }

        public String getStatus() {
            return status;
        }
    }

    public enum Status {
        SUCCESS,
        WARNING,
        ERROR
    }
}

