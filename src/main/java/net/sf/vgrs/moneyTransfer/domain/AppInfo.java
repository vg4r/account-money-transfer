package net.sf.vgrs.moneyTransfer.domain;

public class AppInfo {


    private int port;
    private int contextPath;

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getContextPath() {
        return contextPath;
    }

    public void setContextPath(int contextPath) {
        this.contextPath = contextPath;
    }
}
