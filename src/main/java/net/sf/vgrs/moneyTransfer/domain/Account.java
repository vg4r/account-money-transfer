package net.sf.vgrs.moneyTransfer.domain;

import net.sf.vgrs.moneyTransfer.utlis.Constants;

import java.math.BigDecimal;

public class Account {
    private Long accountNumber;
    private Constants.Currencies currency;
    private long clientId;
    private BigDecimal currentBalance;

    public Account() {
    }

    public Account(long clientId, Constants.Currencies currency) {
        this.clientId = clientId;
        this.currency = currency;
    }

    public Account(Long accountNumber, Constants.Currencies currency, long clientId, BigDecimal currentBalance) {
        this.accountNumber = accountNumber;
        this.currency = currency;
        this.clientId = clientId;
        this.currentBalance = currentBalance;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Constants.Currencies getCurrency() {
        return currency;
    }

    public void setCurrency(Constants.Currencies currency) {
        this.currency = currency;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }
}
