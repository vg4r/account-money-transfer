package net.sf.vgrs.moneyTransfer.service;

import net.sf.vgrs.moneyTransfer.dao.AccountDao;
import net.sf.vgrs.moneyTransfer.dao.ConnectionManager;
import net.sf.vgrs.moneyTransfer.domain.Account;
import net.sf.vgrs.moneyTransfer.domain.MoneyTransactionInfo;
import net.sf.vgrs.moneyTransfer.utlis.Constants;
import net.sf.vgrs.moneyTransfer.utlis.UserDefinedException;
import net.sf.vgrs.moneyTransfer.utlis.UserDefinedException.Errors;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;

public class MoneyTransactionService {

    private AccountDao accountDao;
    private ConnectionManager connectionManager;

    @Inject
    public MoneyTransactionService(
            @Named(Constants.ANNOTATION_JDBC_CONNECTION_MAN)
            ConnectionManager connectionManager,
            @Named(Constants.ANNOTATION_ACCOUNT_DAO_JDBC)
                    AccountDao accountDao ){
        this.accountDao = accountDao;
        this.connectionManager = connectionManager;
    }

    public MoneyTransactionInfo block(Long accountNumber,
                               Long counterPartAccountNumber,
                               BigDecimal amount) throws UserDefinedException{
        try {
            connectionManager.openConnection();
            Account accountInfo = accountDao.getAccountInfo(accountNumber);
            System.out.printf("curr balance %s, amount is %s", accountInfo.getCurrentBalance(), amount);
            System.out.println();
            if (accountInfo.getCurrentBalance().compareTo(amount) == -1){
                throw new UserDefinedException(Errors.ERROR_BUSINESS_LEVEL, "Not enough amount");
            }

            MoneyTransactionInfo blockInfo = accountDao.block(accountNumber, counterPartAccountNumber, amount);

            accountDao.updateAccountBalance(accountNumber, amount.multiply(BigDecimal.valueOf(-1)));
            connectionManager.commit();
            return blockInfo;
        }catch (UserDefinedException e){
            connectionManager.rollback();
            throw e;
        }

    }

    public boolean credit(MoneyTransactionInfo info) throws UserDefinedException{
        connectionManager.openConnection();
        accountDao.credit(info);
        boolean b = accountDao.updateAccountBalance(info.getAccountNumber(), info.getAmount().abs());
        connectionManager.commit();
        connectionManager.closeConnection();
        return b;
    }

    public boolean debit(Long accountNum, Long transactionNumber) throws UserDefinedException{
        connectionManager.openConnection();
        boolean debit = accountDao.debit(accountNum, transactionNumber);
        connectionManager.commit();
        connectionManager.closeConnection();
        return debit;
    }


}
