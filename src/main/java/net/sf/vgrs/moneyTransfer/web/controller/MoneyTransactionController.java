package net.sf.vgrs.moneyTransfer.web.controller;

import net.sf.vgrs.moneyTransfer.dao.AccountDao;
import net.sf.vgrs.moneyTransfer.dao.ConnectionManager;
import net.sf.vgrs.moneyTransfer.domain.Account;
import net.sf.vgrs.moneyTransfer.domain.DebitDetails;
import net.sf.vgrs.moneyTransfer.domain.MoneyTransactionInfo;
import net.sf.vgrs.moneyTransfer.service.MoneyTransactionService;
import net.sf.vgrs.moneyTransfer.utlis.Constants;
import net.sf.vgrs.moneyTransfer.utlis.UserDefinedException;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static net.sf.vgrs.moneyTransfer.utlis.Constants.ANNOTATION_ACCOUNT_DAO_JDBC;
import static net.sf.vgrs.moneyTransfer.utlis.Constants.ANNOTATION_JDBC_CONNECTION_MAN;

@Path("/money-transaction-controller")
public class MoneyTransactionController {

    private ConnectionManager cm;

    private MoneyTransactionService moneyTransactionService;

    private AccountDao accountDao;

    public MoneyTransactionController(){

    }

    @Inject
    public MoneyTransactionController(
            @Named(ANNOTATION_JDBC_CONNECTION_MAN)
                    ConnectionManager cm ,
            @Named(ANNOTATION_ACCOUNT_DAO_JDBC)
                    AccountDao accountDao,
            MoneyTransactionService moneyTransactionService
    ) {
        this.cm = cm;
        this.accountDao = accountDao;
        this.moneyTransactionService = moneyTransactionService;
    }

    @POST
    @Path("/block-amount")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response blockAmount(@HeaderParam(Constants.TAG_REQ_CODE)
                                            String reqCode ,MoneyTransactionInfo moneyTransactionInfo) {

        try {
            MoneyTransactionInfo blockInfo = moneyTransactionService
                    .block(moneyTransactionInfo.getAccountNumber(),
                            moneyTransactionInfo.getCounterPartAccountNumber(),
                            moneyTransactionInfo.getAmount());

            return Response.ok()
                    .header(Constants.TAG_REQ_CODE, reqCode)
                    .entity(blockInfo).build();
        } catch (UserDefinedException e) {
            e.printStackTrace();
            return Response.serverError()
                    .status(Response.Status.BAD_REQUEST)
                    .header(Constants.TAG_STATUS, e.getError().format())
                    .header(Constants.TAG_REQ_CODE, reqCode)
                    .build();
        }
    }


    @POST
    @Path("/credit-amount")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response creditAmount(@HeaderParam(Constants.TAG_REQ_CODE) String reqCode,
                                 MoneyTransactionInfo moneyTransactionInfo) {
        try {
            moneyTransactionService.credit(moneyTransactionInfo);
            return Response.ok()
                    .header(Constants.TAG_REQ_CODE, reqCode)
                    .build();
        } catch (UserDefinedException e) {
            e.printStackTrace();
            return Response.serverError()
                    .status(Response.Status.BAD_REQUEST)
                    .header(Constants.TAG_STATUS, e.getError().format())
                    .header(Constants.TAG_REQ_CODE, reqCode)
                    .build();
        }
    }


    @POST
    @Path("/debit-amount")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response debitAmount(@HeaderParam(Constants.TAG_REQ_CODE) String reqCode,
                                DebitDetails debitDetails) {
        try {
            moneyTransactionService.debit(debitDetails.getAccountNumber(), debitDetails.getTransactionNumber());
            return Response.ok()
                    .header(Constants.TAG_REQ_CODE, reqCode)
                    .build();
        } catch (UserDefinedException e) {
            e.printStackTrace();
            return Response.serverError()
                    .status(Response.Status.BAD_REQUEST)
                    .header(Constants.TAG_STATUS, e.getError().format())
                    .header(Constants.TAG_REQ_CODE, reqCode)
                    .build();
        }
    }




}
