package net.sf.vgrs.moneyTransfer.web;

import net.sf.vgrs.moneyTransfer.utlis.Constants;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.UUID;

@Provider
@PreMatching
public class RequestCodeGeneratorFilter implements ContainerRequestFilter {
    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        containerRequestContext.getHeaders().add(Constants.TAG_REQ_CODE,
                UUID.randomUUID().toString());
    }
}
