# Money transfer rest api between 2 accounts

## Getting Started

### Prerequisites

* **Git client**
* **Jdk 8+**
* **maven**


### Clone and run project
```
git clone https://gitlab.com/vgrs/account-money-transfer.git
cd account-money-transfer/
mvn clean test tomcat8:run
```

### Rest api

##### Endpoints

###### http://localhost:8080/money-transfer/client-controller/add-client
```
POST /money-transfer/client-controller/add-client HTTP/1.1
Host: <host>:<port>
Content-Type: application/json
{
	"fullName": "<full name>"
}
```

###### http://localhost:8080/money-transfer/client-controller/add-client
```
POST /money-transfer/client-controller/add-account HTTP/1.1
Host: <host>:<port>
Content-Type: application/json
{
    "clientId": <client id>,
    "currency": <"USD"|"PLN">
}
```

###### http://localhost:8080/money-transfer/client-controller/add-account
```
POST /money-transfer/client-controller/add-account HTTP/1.1
Host: <host>:<port>
Content-Type: application/json
{
    "clientId": <client id>,
    "currency": <"USD"|"PLN">
}
```

###### http://localhost:8080/money-transfer/client-controller/get-account-info
```
GET /money-transfer/client-controller/get-account-info HTTP/1.1
Host: <host>:<port>
```


###### http://localhost:8080/money-transfer/money-transaction-controller/block-amount
```
POST /money-transfer/money-transaction-controller/block-amount HTTP/1.1
Host: <host>:<port>
Content-Type: application/json
{
    "accountNumber": <account number>,
    "counterPartAccountNumber": <counterpart account number>,
    "amount" : <amount>
}
```

###### http://localhost:8080/money-transfer/money-transaction-controller/credit-amount
```
POST /money-transfer/money-transaction-controller/credit-amount HTTP/1.1
Host: <host>:<port>
Content-Type: application/json
{
    "transactionNumber": <transaction number returned from /money-transaction-controller/block-amount service>,
    "accountNumber": <account number>,
    "counterPartAccountNumber": <counterpart account number>,
    "amount" : <amount>
}
```


###### http://localhost:8080/money-transfer/money-transaction-controller/debit-amount
```
POST /money-transfer/money-transaction-controller/credit-amount HTTP/1.1
Host: <host>:<port>
Content-Type: application/json
{
    "transactionNumber": <transaction number>,
    "accountNumber": <account number>
}
```